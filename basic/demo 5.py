
def f1():
    num=list()
    print("Type of num: ",num)

f1()

def f2():
    num=[10,20,30,40,50]
    print("Type of num: ",num)
    for tmp in num:
        print(tmp)

    print("Element at -1 position: ",num[-1])
    print("slicing: ", num[0:3])
    print("slicing: ", num[3:5])
    print("40 is present on index = ", num.index(40)+1)
    print("slicing with a gap of 2",num[0:5:2])

f2()

def f3():
    countries = ["india", "usa", "uk", "russia"]
    print("\n",countries)
    countries.append("germany")
    print(countries)
    country = countries.pop()
    print("popped value =", country)
    print(countries)
    countries.insert(3,"France")
    print(countries)
    print("Number of country in countries: ",len(countries))
    countries[3]="Japan"
    print(countries)

f3()

def function4():
    persons = []

    index_values = list(range(0, 3, 1))
    for index in index_values:
        # to get an input from user
        p1 = input("enter person name: ")
        persons.append(p1)

    print(persons)

function4()

def function5():
    t1=(10,11,12,13,14,15)
    print(t1)
    del(t1)


function5()

def f6():
    person1 = ["person1", "person1@test.com", 30, True]
    print(person1)
    for info in person1:
        print("info =", info)
        print("Type of info =", type(info))

    print("name =", person1[0])
    print("email =", person1[1])
    print("age =", person1[2])
    print("eligible for voting =", person1[3])

    person2 = ("person2", 30, "person2@test.com", True)

    print("\nname =", person2[0])
    print("email =", person2[1])
    print("age =", person2[2])
    print("eligible for voting =", person2[3])

f6()

def f7():
    person1={
        "name":"A",
        "email":"A123@test.com",
        "age":10,
        "canVote":False
    }
    print("\nname =", person1["name"])
    print("email =", person1["email"])
    print("age =", person1["age"])
    print("canVote =", person1["canVote"])
    if person1["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")

    person2 = {
        "name": "B",
        "email": "B123@test.com",
        "age": 60,
        "canVote":True
    }
    print("\nname =", person1["name"])
    print("email =", person1["email"])
    print("age =", person1["age"])
    print("canVote =", person1["canVote"])
    if person2["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")
    person2["name"] = "person3"
    print("name =", person2["name"])
    print("Keys of person 1=",person1.keys())
    print("Values of person 1=", person1.values())

f7()

def function8():
    # list
    numbers_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]

    # tuple
    numbers_2 = (10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50)

    # set
    numbers_3 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}

    print(numbers_1)
    print("type of numbers_1 =", type(numbers_1))

    print(numbers_2)
    print("type of numbers_2 =", type(numbers_2))

    print(numbers_3)
    print("type of numbers_3 =", type(numbers_3))

function8()

def function9():
    # list of dictionaries
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5}, # 0
        {"model": "i10", "company": "hyundai", "price": 5.5}, # 1
        {"model": "nano", "company": "tata", "price": 1.5}    # 2
    ]



    for car in cars:

        print("model =", car["model"])
        print("company =", car.get("company"))
        print("price =", car["price"])
        print()

    print("company =", cars[2]["company"])
    print("company =", cars[2].get("company"))

function9()

def function10():
    # list of tuples
    # collection of collections => multi-dimensional collection
    mobiles = [
        ("iphone xs max", "apple", 144000),
        ("z10", "blackberry", 40000),
        ("galaxy s10", "samsung", 78000)
    ]

    # for mobile in mobiles:
    #     # print(mobile)
    #     print("model =", mobile[0])
    #     print("company =", mobile[1])
    #     print("price =", mobile[2])
    #     print()

    print("\nModel = ", mobiles[0][0])
    print("\nCompany = ", mobiles[0][1])
    print("\nModel = ", mobiles[1][0])
    print("\nPrice = ", mobiles[1][2])

function10()
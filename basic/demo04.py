def f1(p1):
    print("Inside function 1")
    print("p1: ",p1)

def f2(p1,p2):
    print("Inside function 2")
    print("p1=",p1)
    print("p2=",p2)

f1(2)
f2(2,'a')

def f3(p1=0,p2=0):
    print("Inside function 3")
    print("p1=",p1)
    print("p2=",p2)

f3()
f3(32.23)
f3(2,'a')


# Operation on list

colors=["red","blue","magenta","green"]
print("Colors:",colors)
colors.append("grey")
print("Adding grey to list:",colors)
colors.insert(2,"pink")
print("Adding pink at index 2:",colors)
color=colors.pop()
print("Removed last element of list: ",color)
print("\n",colors)
color=colors.pop(1)
print("After removing element from index 1:",colors)

print("Length of list colors:",len(colors))
colors[2]="Peach"
print("List after replacing magenta with peach: ",colors)
import question8main as calc

a=int(input("Enter 1st number:"))
b=int(input("Enter 2nd number:"))

print(f"{a}+{b}={calc.add(a,b)}")
print(f"{a}-{b}={calc.sub(a,b)}")
print(f"{a}*{b}={calc.mul(a,b)}")
print(f"{a}/{b}={calc.floatdiv(a,b)}")
print(f"{a}//{b}={calc.intdiv(a,b)}")

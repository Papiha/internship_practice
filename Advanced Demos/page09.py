import page08 as mypage
mypage.add(10,30)

def add(p1,p2):
    print("\nInside Page 09")
    print(f"{p1}+{p2}={p1+p2}")

add(10,8)

from page08 import add as myadd
myadd(40,51)

import product
import faculty

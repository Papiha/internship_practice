import math

print(f"pi={math.pi}")
print(f"e={math.e}")
print(f"2^14={math.pow(2,14)}")
print(f"2^32={math.pow(2,32)}")
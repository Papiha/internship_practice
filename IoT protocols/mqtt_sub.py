
import paho.mqtt.client as mqtt

def on_connect(client, userdata, flags, rc):
    print("MQTT Client Connected.")

def on_message(client, userdata, msg):
    data = msg.payload.decode("utf-8")
    print("MQTT Data Received : " + msg.topic + ":Temperature -->" + data)


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("mosquitto.org", 1883, 60)
client.subscribe("LM35_t")

print("subscriber waiting for message ...")
client.loop_forever()
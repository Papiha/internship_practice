from flask import Flask, jsonify, request, abort

app = Flask(__name__)

people = [
        {"id": "p1", "name": "Nilesh", "age": 37, "addr": "Mumbai"},
        {"id": "p2", "name": "Devendra", "age": 30, "addr": "Pune"},
        {"id": "p3", "name": "Abhijeet", "age": 27, "addr": "Nagpur"},
        {"id": "p4", "name": "Satara", "age": 26, "addr": "Satara"}
]
temp=[]

@app.route("/hello")
def hello_world():
    return "Hello World!"

@app.route("/persons")
def get_all_persons():
    return jsonify(people)




@app.route("/persons/<id>", methods = ["GET"])
def get_person(id):
    for p in people:
        if p["id"] == id:
            return p
    return jsonify(None)

@app.route("/persons", methods = ["POST"])
def add_person():
    if not request.json:
        abort(400)
    people.append(request.json)
    return {"status": "success"}
@app.route("/lm")
def get_temp():
    return jsonify(temp)
@app.route("/lm", methods= ["GET"])
def get_temp1():
    for t in temp:
        return t
    return jsonify(None)
@app.route("/lm",methods = ["POST"])
def add_temp():
    if not request.json:
        abort(400)
    temp.append(request.json)
    return {"status": "success"}

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=5000,debug=True)




